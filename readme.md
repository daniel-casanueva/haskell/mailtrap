
## mailtrap

This library supplies functions to use the
[Mailtrap API](https://api-docs.mailtrap.io/docs/mailtrap-api-docs)
from a Haskell project.

It implements both email sending and testing.
