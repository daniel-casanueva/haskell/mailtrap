## 0.1.2.1
* Add compatibility with base64-1.0.

## 0.1.2.0
* Add `FromJSON` and `ToJSON` instances for the `Token` type.

## 0.1.1.0
* Export types: `EmailAddress`, `Inbox`, `InboxMessage`.

## 0.1.0.0
* First release.
